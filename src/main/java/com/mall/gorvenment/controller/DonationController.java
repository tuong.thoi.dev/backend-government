package com.mall.gorvenment.controller;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.entity.Donation;
import com.mall.gorvenment.service.DonationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/donation")
@Slf4j
public class DonationController {
    @Autowired
    private DonationService donationService;

    @GetMapping("/getall")
    public ResponseEntity<List<Donation>> getAllDonation(){
        return ResponseEntity.ok().body(donationService.getAll());
    }

    @GetMapping("/getbyid/{id}")
    public ResponseEntity<DonationResponseDto> getDonation(@PathVariable UUID id){
        return ResponseEntity.ok().body(donationService.getByUUID(id));
    }

    @PostMapping
    public ResponseEntity<DonationResponseDto> save(@RequestBody DonationRequestDto requestDto){
        log.info(requestDto.getDescription());
        return ResponseEntity.ok().body(donationService.save(requestDto));
    }

    @PostMapping("/disable/{id}")
    public ResponseEntity<DonationResponseDto> disable(@PathVariable UUID id){
        return ResponseEntity.ok().body(donationService.disable(id));
    }
}
