package com.mall.gorvenment.controller;

import com.mall.gorvenment.dto.request.PaymentRequestDto;
import com.mall.gorvenment.dto.response.PaymentResponseDto;
import com.mall.gorvenment.service.PaymentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;
    @Autowired
    ModelMapper mapper;

    @PostMapping
    public PaymentResponseDto save(@RequestBody PaymentRequestDto request){
        return paymentService.save(request);
    }
}
