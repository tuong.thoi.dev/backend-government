package com.mall.gorvenment.controller;

import com.mall.gorvenment.dto.request.DonationUserRequestDto;
import com.mall.gorvenment.dto.response.CustomDonationUserResponseDto;
import com.mall.gorvenment.dto.response.DonationUserResponseDto;
import com.mall.gorvenment.entity.Donation;
import com.mall.gorvenment.service.DonationUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/donationuser")
@Slf4j
public class DonationUserController {
    @Autowired
    private DonationUserService donationUserService;

    @GetMapping("/donation/{id}")
    public List<DonationUserResponseDto> getAllDonation(@PathVariable UUID id){
        return donationUserService.getAllByDonationId(id);
    }

    @GetMapping("/user/{id}")
    public List<DonationUserResponseDto> getAllDonationByUser(@PathVariable UUID id){
        return donationUserService.getByUserId(id);
    }

    @PostMapping
    public DonationUserResponseDto save(@RequestBody DonationUserRequestDto request){
        return donationUserService.save(request);
    }

    @GetMapping("/donation/top10/{id}")
    public List<CustomDonationUserResponseDto> getAllTop10Donation(@PathVariable UUID id){
        return donationUserService.getTop10Donation(id);
    }
}
