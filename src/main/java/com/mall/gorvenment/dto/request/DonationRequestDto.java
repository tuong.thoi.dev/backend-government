package com.mall.gorvenment.dto.request;


import lombok.*;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class DonationRequestDto {
    private String name;
    private String description;
    private long totalDonation;
    private Date createDate;
    private Date dueDate;
    private boolean isEnabled;
}
