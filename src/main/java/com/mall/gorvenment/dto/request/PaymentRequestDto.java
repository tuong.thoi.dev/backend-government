package com.mall.gorvenment.dto.request;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentRequestDto {
    private String typeOfCard;
    private boolean isSuccess;
    private UUID donationUserId;
}
