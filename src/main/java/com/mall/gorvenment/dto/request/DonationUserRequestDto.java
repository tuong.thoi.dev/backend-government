package com.mall.gorvenment.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class DonationUserRequestDto {
    private long donation;
    private Date date;
    private UUID donationId;
    private String username;
    private String message;
}
