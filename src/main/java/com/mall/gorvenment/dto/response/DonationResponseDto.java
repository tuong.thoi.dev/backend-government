package com.mall.gorvenment.dto.response;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class DonationResponseDto {
    private UUID id;
    private String name;
    private String description;
    private long totalDonation;
    private Date createDate;
    private Date dueDate;
    private boolean isEnabled;
    private long numberOfDonation;

}
