package com.mall.gorvenment.dto.response;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentResponseDto {
    private UUID id;
    private String typeOfCard;
    private boolean isSuccess;
}
