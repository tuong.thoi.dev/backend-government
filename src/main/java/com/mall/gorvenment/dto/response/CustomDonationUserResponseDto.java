package com.mall.gorvenment.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mall.gorvenment.entity.User;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CustomDonationUserResponseDto {
    private UUID id;
    private long donation;
    private Date date;
    private String message;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private User user;
}
