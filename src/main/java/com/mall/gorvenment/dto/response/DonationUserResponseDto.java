package com.mall.gorvenment.dto.response;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class DonationUserResponseDto {
    private UUID id;
    private long donation;
    private Date date;
    private String message;
}
