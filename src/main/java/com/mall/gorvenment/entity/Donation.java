package com.mall.gorvenment.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Donation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false)
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    private long totalDonation;
    //@JsonFormat(pattern="YYYY-MM-dd")
    @Column(name="`create_date`")
    private Date createDate;
    //@JsonFormat(pattern="YYYY-MM-dd")
    @Column(name = "`due_date`")
    private Date dueDate;
    @Column(name="`is_enabled")
    private boolean isEnable;
    @Column(name = "`number_of_donation`")
    private long numberOfDonation = 0;
    @JsonIgnore
    @OneToMany(mappedBy = "donation", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DonationUser> donationUsers = new ArrayList<>();
}
