package com.mall.gorvenment.expections;

import com.mall.gorvenment.dto.response.ExpectionResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class BadRequestExpection extends RuntimeException{
    private final HttpStatus STATUS;

    public BadRequestExpection(String message) {
        super(message);
        STATUS = HttpStatus.BAD_REQUEST;

    }
}
