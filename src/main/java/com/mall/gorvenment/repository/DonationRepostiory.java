package com.mall.gorvenment.repository;

import com.mall.gorvenment.entity.Donation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DonationRepostiory extends JpaRepository<Donation, UUID> {
    List<Donation> findAll();
}
