package com.mall.gorvenment.repository;

import com.mall.gorvenment.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByUsername(String username);
    Optional<User> findByUsernameAndPassword(String username, String password);
    User findByUsernameOrEmail(String username, String email);
    Optional<User> findById(UUID uuid);
}
