package com.mall.gorvenment.repository;

import com.mall.gorvenment.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
    @Query("SELECT p FROM Payment p WHERE p.donationUser.id = :donationId")
    Payment findByDonationUserID(@Param("donationId") UUID id);
}
