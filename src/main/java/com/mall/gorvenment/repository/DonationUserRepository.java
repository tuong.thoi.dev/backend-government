package com.mall.gorvenment.repository;

import com.mall.gorvenment.entity.DonationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface DonationUserRepository extends JpaRepository<DonationUser, UUID> {
    List<DonationUser> findByDonations_IdOrderByDateDesc(UUID id);
    List<DonationUser> findByUserId(UUID id);
    List<DonationUser> findByDonations_Id(UUID id);

}
