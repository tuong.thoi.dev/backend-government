package com.mall.gorvenment.service;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.request.DonationUserRequestDto;
import com.mall.gorvenment.dto.response.CustomDonationUserResponseDto;
import com.mall.gorvenment.dto.response.DonationUserResponseDto;
import com.mall.gorvenment.entity.DonationUser;

import java.util.List;
import java.util.UUID;

public interface DonationUserService {
    DonationUserResponseDto save(DonationUserRequestDto request);
    List<DonationUserResponseDto> getByUserId(UUID id);
    List<DonationUserResponseDto> getAllByDonationId(UUID id);
    List<CustomDonationUserResponseDto> getTop10Donation(UUID id);
}
