package com.mall.gorvenment.service.impl;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.request.DonationUserRequestDto;
import com.mall.gorvenment.dto.response.CustomDonationUserResponseDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.dto.response.DonationUserResponseDto;
import com.mall.gorvenment.entity.Donation;
import com.mall.gorvenment.entity.DonationUser;
import com.mall.gorvenment.entity.Payment;
import com.mall.gorvenment.entity.User;
import com.mall.gorvenment.repository.DonationRepostiory;
import com.mall.gorvenment.repository.DonationUserRepository;
import com.mall.gorvenment.repository.PaymentRepository;
import com.mall.gorvenment.repository.UserRepository;
import com.mall.gorvenment.service.DonationUserService;
import com.mall.gorvenment.service.PaymentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DonationUserServiceImpl implements DonationUserService {
    @Autowired
    private DonationUserRepository donationUserRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DonationRepostiory donationRepostiory;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public DonationUserResponseDto save(DonationUserRequestDto request) {
        DonationUser entity = mapper.map(request, DonationUser.class);
        User userEntity =  userRepository.findByUsername(request.getUsername()).get();
        Donation donationEntity = donationRepostiory.findById(request.getDonationId()).get();
        entity.setDonations(donationEntity);
        entity.setUser(userEntity);
        entity = donationUserRepository.save(entity);
        DonationUserResponseDto response = mapper.map(entity, DonationUserResponseDto.class);

        return response;
    }

    @Override
    public List<DonationUserResponseDto> getByUserId(UUID id) {
        List<DonationUser> donationUserList = donationUserRepository.findByUserId(id);
        return donationUserList.stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DonationUserResponseDto> getAllByDonationId(UUID id) {
        List<DonationUser> donationUserList = donationUserRepository.findByDonations_Id(id);
        return donationUserList.stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CustomDonationUserResponseDto> getTop10Donation(UUID id) {
        List<DonationUser> donationUserList = donationUserRepository.findByDonations_Id(id);
        Collections.sort(donationUserList, new Comparator<DonationUser>() {
            @Override
            public int compare(DonationUser o1, DonationUser o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        List<DonationUser> entity = new ArrayList<>();
        for(DonationUser du:donationUserList){
            Payment entityPayment = paymentRepository.findByDonationUserID(du.getId());
            if(entityPayment.isSuccess() && entity.size() <= 10){
                entity.add(du);
            }
        }
        List<CustomDonationUserResponseDto> responseDto = entity.stream()
                .map(this::mapToCustomDto)
                .collect(Collectors.toList());
        List<CustomDonationUserResponseDto> response = new ArrayList<>();
        for(CustomDonationUserResponseDto c : responseDto){
            c.setFirstName(c.getUser().getFirstName());
            c.setLastName(c.getUser().getLastName());
            c.setUser(new User());
            response.add(c);
        }
        return response;
    }

    public DonationUserResponseDto mapToDto(DonationUser donationUser){
        return mapper.map(donationUser, DonationUserResponseDto.class);
    }
    public CustomDonationUserResponseDto mapToCustomDto(DonationUser donationUser){
        return mapper.map(donationUser, CustomDonationUserResponseDto.class);
    }
}
