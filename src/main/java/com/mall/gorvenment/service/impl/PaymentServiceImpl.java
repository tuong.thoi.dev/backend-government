package com.mall.gorvenment.service.impl;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.request.PaymentRequestDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.dto.response.PaymentResponseDto;
import com.mall.gorvenment.entity.Donation;
import com.mall.gorvenment.entity.Payment;
import com.mall.gorvenment.repository.DonationRepostiory;
import com.mall.gorvenment.repository.DonationUserRepository;
import com.mall.gorvenment.repository.PaymentRepository;
import com.mall.gorvenment.service.PaymentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private DonationUserRepository donationUserRepository;
    @Autowired
    private DonationRepostiory donationRepostiory;
    @Autowired
    private ModelMapper mapper;

    @Override
    public PaymentResponseDto save(PaymentRequestDto dto) {
        Payment entity = new Payment();
        entity.setSuccess(dto.isSuccess());
        entity.setTypeOfCard(dto.getTypeOfCard());
        entity.setDate(new Date());
        entity.setDonationUser(donationUserRepository.findById(dto.getDonationUserId()).get());
        entity = paymentRepository.save(entity);
        PaymentResponseDto paymentDto = mapper.map(entity, PaymentResponseDto.class);
        if(paymentDto.isSuccess()){
            Optional<Donation> donation = donationRepostiory.findById(entity.getDonationUser().getDonations().getId());
            Donation donationEntity = donation.get();
            donationEntity.setTotalDonation(donationEntity.getTotalDonation() +
                    donationUserRepository.findById(dto.getDonationUserId()).get().getDonation());
            donationEntity.setNumberOfDonation(donationEntity.getNumberOfDonation() + 1);
            donationRepostiory.save(donationEntity);
        }
        return paymentDto;
    }

    @Override
    public PaymentResponseDto getByDonationUserId(UUID id) {
        Payment entity = paymentRepository.findByDonationUserID(id);
        PaymentResponseDto dto = mapper.map(entity, PaymentResponseDto.class);
        return dto;
    }
}
