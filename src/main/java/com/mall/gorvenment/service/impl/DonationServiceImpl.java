package com.mall.gorvenment.service.impl;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.entity.Donation;
import com.mall.gorvenment.repository.DonationRepostiory;
import com.mall.gorvenment.service.DonationService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class DonationServiceImpl implements DonationService {
    @Autowired
    private DonationRepostiory donationRepostiory;
    @Autowired
    private ModelMapper mapper;

    @Override
    public DonationResponseDto save(DonationRequestDto request) {
        Donation entity = mapper.map(request, Donation.class);
        entity.setEnable(request.isEnabled());
        entity = donationRepostiory.save(entity);
        DonationResponseDto responseDto = mapper.map(entity, DonationResponseDto.class);
        responseDto.setEnabled(entity.isEnable());
        return responseDto;
    }

    @Override
    public List<Donation> getAll() {
        List<Donation> entities = donationRepostiory.findAll();
        return entities;
    }

    @Override
    public DonationResponseDto getByUUID(UUID id) {
        Optional<Donation> donation = donationRepostiory.findById(id);
        if(!donation.isEmpty()){
            DonationResponseDto responseDto = mapper.map(donation.get(), DonationResponseDto.class);
            responseDto.setEnabled(donation.get().isEnable());
            return responseDto;
        }
       return null;
    }

    @Override
    public void checkDateDonation(Donation request) {
        Optional<Donation> donation = donationRepostiory.findById(request.getId());
        if(!donation.isEmpty()){
            Date today = new Date();
            Date dueDate = donation.get().getDueDate();
            if(today.compareTo(dueDate) > 0){
                Donation entity = donation.get();
                entity.setEnable(false);
                return;
            }
        }
    }

    @Override
    public DonationResponseDto disable(UUID id) {
        Optional<Donation> donation = donationRepostiory.findById(id);
        Donation entity = donation.get();
        entity.setEnable(false);
        entity = donationRepostiory.save(entity);
        DonationResponseDto responseDto = mapper.map(entity, DonationResponseDto.class);
        return responseDto;
    }

    public void dummpy(){

    }
}
