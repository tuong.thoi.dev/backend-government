package com.mall.gorvenment.service;

import com.mall.gorvenment.entity.Role;
import com.mall.gorvenment.entity.User;
import com.mall.gorvenment.dto.request.UserLoginRequestDto;

import com.mall.gorvenment.dto.request.UserResigterRequestDto;
import com.mall.gorvenment.dto.response.UserLoginResponseDto;
import com.mall.gorvenment.dto.response.UserResigterResponseDto;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUser();
    UserLoginResponseDto login(UserLoginRequestDto userLoginDto);
    UserResigterResponseDto resigter(UserResigterRequestDto userResigterRequestDto, String roleName);
}
