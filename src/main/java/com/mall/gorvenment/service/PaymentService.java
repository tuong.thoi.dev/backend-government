package com.mall.gorvenment.service;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.request.PaymentRequestDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.dto.response.PaymentResponseDto;

import java.util.UUID;

public interface PaymentService {
    PaymentResponseDto save(PaymentRequestDto dto);
    PaymentResponseDto getByDonationUserId(UUID id);
}
