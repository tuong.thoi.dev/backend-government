package com.mall.gorvenment.service;

import com.mall.gorvenment.dto.request.DonationRequestDto;
import com.mall.gorvenment.dto.response.DonationResponseDto;
import com.mall.gorvenment.entity.Donation;

import java.util.List;
import java.util.UUID;

public interface DonationService {
    DonationResponseDto save(DonationRequestDto request);
    List<Donation> getAll();
    DonationResponseDto getByUUID(UUID id);
    void checkDateDonation(Donation requestDto);
    DonationResponseDto disable(UUID id);
}
