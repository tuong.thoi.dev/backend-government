# First container: Build dependencies
# Use an existing image as the base image
FROM maven:3.6.3-jdk-11-slim

# Set the working directory in the container
WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml .

# Build the project dependencies
RUN mvn dependency:go-offline -B

CMD ["mvn","clean","package","-DskipTests"]

# Second container: Run Java project
# Use an existing image as the base image
FROM openjdk:11-jre-slim

# Copy the dependencies built in the first container
COPY --from=0 /root/.m2/repository /root/.m2/repository

# Set the working directory in the container
WORKDIR /app

# Copy the rest of the project files to the container
COPY . .



# Specify the command to run when the container starts
CMD ["java", "-jar", "target/elite-1.1.0.jar"]